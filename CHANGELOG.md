# Changelog

All notable changes to this project will be documented in this file.

## [Unreleased]

## [1.2.4] - 2024-08-16

### Added

-   add backstage metadata files
-   add .gitattributes file

## [1.2.3] - 2024-05-13

### Changed

-   Update Node.js to v20.12.2 Express to v4.19.2 ([core#102](https://gitlab.com/operator-ict/golemio/code/modules/core/-/issues/102))

## [1.2.2] - 2024-04-15

### Changed

-   Update protobufjs to ^7.2.6

## [1.2.0] - 2023-07-13

### Changed

-   Update protobufjs to ^7.2.4

## [1.1.1] - 2023-04-26

### Added

-   TS types generated using pbts ([pid#235](https://gitlab.com/operator-ict/golemio/code/modules/pid/-/issues/235))

## [1.1.0] - 2023-01-23

### Changed

-   Migrate to npm

