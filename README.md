<div align="center">
<p>
    <a href="https://operatorict.cz">
        <img src="https://gitlab.com/operator-ict/golemio/code/modules/core/-/raw/development/.assets/oict_logo.png" alt="oict" width="100px" height="100px" />
    </a>
    <a href="https://golemio.cz">
        <img src="https://gitlab.com/operator-ict/golemio/code/modules/core/-/raw/development/.assets/golemio_logo.png" alt="golemio" width="100px" height="100px" />
    </a>
</p>
</div>

# Golemio OVapi GTFS Realtime Bindings

GTFS Realtime bindings with OVapi extension compiled to JS format.

Based on [gtfs-realtime-bindings](https://github.com/MobilityData/gtfs-realtime-bindings/)
with the addition of OVApi extension (1003).

GTFS RT Extension Registry: https://developers.google.com/transit/gtfs-realtime/guides/extensions
